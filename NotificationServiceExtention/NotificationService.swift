//
//  NotificationService.swift
//  NotificationServiceExtention
//
//  Created by Avi Sapir on 31/01/2022.
//  Copyright © 2022 aviza. All rights reserved.
//

import UserNotifications
//import Firebase

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        print("didReceive")
        if let bestAttemptContent = bestAttemptContent {
            // Modify the notification content here...
            bestAttemptContent.title = "\(bestAttemptContent.title) [modified]"
            //bestAttemptContent.userInfo["data"]
            
            print(bestAttemptContent.userInfo)
            
            //threadIdentifier
            if bestAttemptContent.userInfo.keys.contains("threadIdentifier") {
                if let threadIdentifier = bestAttemptContent.userInfo["threadIdentifier"] as? String {
                    print("threadIdentifier: \(threadIdentifier)" )
                    bestAttemptContent.threadIdentifier = threadIdentifier
                    bestAttemptContent.categoryIdentifier = threadIdentifier
                    //bestAttemptContent.targetContentIdentifier = threadIdentifier
                    //UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [threadIdentifier])


                }
            }
            
//            let fileURL: URL = URL(string: "https://www.cryptocompare.com/media/1382471/euc.png")!
//            let attachement = try? UNNotificationAttachment(identifier: "attachment", url: fileURL, options: nil)
//            bestAttemptContent.attachments = [attachement!]
            
            let fileUrl = URL(string: "https://www.cryptocompare.com/media/1382471/euc.png")!

//            do {
//                let attachment = try UNNotificationAttachment(identifier : "image", url: fileUrl, options: nil)
//                    bestAttemptContent.attachments = [attachment]
//            }
//            catch {
//
//            }
            
            do {
                guard let imageData = NSData(contentsOf:fileUrl) else { return  }
                let att = try UNNotificationAttachment(identifier: fileUrl.path, url: fileUrl, options: nil)
                  bestAttemptContent.attachments = [att]
            }
            catch {
                
            }

         
            
            
            
            
         
            
            let center = UNUserNotificationCenter.current()
            
//            let deafultCategory = UNNotificationCategory(identifier: "CustomSamplePush", actions: [], intentIdentifiers: [], options: [])
//             center.setNotificationCategories(Set([deafultCategory]))
            
//            if #available(iOSApplicationExtension 12.0, *) {
//                let summaryFormat = "%u more messages"
//                let catgory = UNNotificationCategory(identifier: "mycatgory-id",
//                                                     actions: [],
//                                                     intentIdentifiers: [],
//                                                     hiddenPreviewsBodyPlaceholder: nil,
//                                                     categorySummaryFormat: summaryFormat,
//                                                     options: [])
//
//                let center = UNUserNotificationCenter.current()
//                    center.setNotificationCategories([catgory])
//
//            } else {
//                // Fallback on earlier versions
//            }
            
            
            
//            bestAttemptContent.threadIdentifier = "F39-C521-A7A"
//            bestAttemptContent.categoryIdentifier = "avi zaza"
//            if #available(iOSApplicationExtension 12.0, *) {
//                bestAttemptContent.summaryArgument = "avi zaza"
//                bestAttemptContent.summaryArgumentCount = 1
//            } else {
//                // Fallback on earlier versions
//            }

//            if let category =  bestAttemptContent.userInfo["Nick"] as? String {
//                bestAttemptContent.categoryIdentifier = category
//                print(" \(category) ✅")
//                bestAttemptContent.summaryArgument =
//
//            }
//
//            if let thread =  bestAttemptContent.userInfo["Nick"] as? String {
//                bestAttemptContent.threadIdentifier = thread
//            }
            
        //    bestAttemptContent.categorySummaryFormat
//            if #available(iOSApplicationExtension 12.0, *) {
//                bestAttemptContent.summaryArgumentCount = 1
//            } else {
//                // Fallback on earlier versions
//            }
            
      
                


            contentHandler(bestAttemptContent)
          // [[FIRMessaging extensionHelper] populateNotificationContent:self.bestAttemptContent
              //                                         withContentHandler:contentHandler];
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}
