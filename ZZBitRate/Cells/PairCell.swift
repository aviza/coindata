//
//  PairCell.swift
//  ZZBitRate
//
//  Created by Oren on 04/03/2021.
//  Copyright © 2021 aviza. All rights reserved.
//

import UIKit

class PairCell: UITableViewCell {

    
    @IBOutlet weak var pairNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
