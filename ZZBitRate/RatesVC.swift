//
//  RatesVC.swift
//  ZZBitRate
//
//  Created by aviza on 11/12/2017.
//  Copyright © 2017 aviza. All rights reserved.
//

import UIKit

import SocketIO

import Kingfisher

import Starscream

import RealmSwift

import StoreKit

import Firebase

import RxRelay
import RxSwift
import AppTrackingTransparency



class RatesVC: BannerViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate,ChooceCoinTVCDelegate {
        
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalVolLabel: UILabel!
    @IBOutlet weak var totalMarketCapLabel: UILabel!
    @IBOutlet weak var tableHeaderView: UIView!
    var selectedCoin : ZZCoinRate?
    
    var dataSource : Array<ZZCoinRate> = []
    //var filteredData : Array<ZZCoin> = []

    var ccSubscriptions: [CCSubscription] = []
    var subscriptionsLoadComplete = false
    
    var timer : Timer?
    
    private let refreshControl = UIRefreshControl()
    private var pageIndex = 0
    
    //var searchBar:UISearchBar = UISearchBar(frame : CGRect(x:0,y:0, width:200, height:21))

    var socket: WebSocket!
    var socket2: WebSocket!

    var platformDataError: Error?
    
    @IBOutlet weak var timeSegmented: UISegmentedControl!
    var currencySelectionItem: UIBarButtonItem!
    
    func showInAppPurchasesVC() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IAP_VC") as! IAP_VC
               vc.modalPresentationStyle = .overCurrentContext
               self.present(vc, animated: true, completion: nil)
    }


    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        refreshControl.tintColor = Color.white

        self.tableView.tableFooterView = UIView()
        
        setNavToNonSearchMode()
        

        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("applicationDidBecomeActive"), object: nil)
        
        
        if let userId = User.user()?.userId {
            Analytics.setUserID(userId)
        }
        if !UserDataManager.shared.visitedPurchaseScreen() {
            Analytics.setUserProperty("Not Visited", forName: "Visited_Purchase_Screen")
        }
        if !UserDataManager.shared.userMarkedAsPro() {
            Analytics.setUserProperty("Regular", forName: "User_Type")
        }
        
        setTimeSegmentedUI()

        fetchData()

        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        refreshControl.addTarget(self, action: #selector(refreshControlValueChanged(_:)), for: .valueChanged)
        
        AppStoreReviewManager.requestReviewIfAppropriate()
        
        subscribeToCurrencyChange()
    }

    // MARK: - Currency Change
    func subscribeToCurrencyChange() {
        
        UserDataManager.shared.userCurrency.asObservable().subscribe(onNext: { [weak self] (currency) in
            self?.currencySelectionItem.title = currency
            self?.getCoinsDataAndReloadTable(page: 0)
        }, onError: { error in
            print("Observable Error: \(error)")}, onCompleted: nil, onDisposed: nil)
        .disposed(by: disposeBag)

    }
    
    @objc func selectCurrency(sender: UIBarButtonItem) {
        let currencySelectionVC = SelectCurrencyVC(nibName: "SelectCurrencyVC", bundle: nil)
        currencySelectionVC.preferredContentSize = CGSize(width: 160, height: 200)
        currencySelectionVC.modalPresentationStyle = .popover
        currencySelectionVC.popoverPresentationController?.barButtonItem = currencySelectionItem
        currencySelectionVC.popoverPresentationController?.delegate = self
        present(currencySelectionVC, animated: true, completion: nil)
        
    }
        
    //MARK: ************************ TimeSegmented *************************************
    func setTimeSegmentedUI() {
        timeSegmented.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        timeSegmented.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        
         let index = UserDefaults.standard.integer(forKey: "RatesVCTimeSegmentdValue")
         timeSegmented.selectedSegmentIndex = index
        
        
        
    }
    
    @IBAction func timeSegmentedValueChange(_ sender: Any) {
        
        let segmented = sender as! UISegmentedControl
        UserDefaults.standard.setValue(segmented.selectedSegmentIndex, forKey: "RatesVCTimeSegmentdValue")
        
        self.tableView.reloadData()

    }
    
    //MARK: ************************ FetchData *************************************

    func fetchData() {
        
        if UserDataManager.shared.needToFetchAllData(userDefaults: UserDefaults.standard) {
            
                getAllPlatformData()
        }
        else {
            self.showLoading()
            self.getFirstPageOfCoinsAndStartTimer()
        }
        
    }
    
    
    
    
    
    func getAllPlatformData()  {
            
        showLoading()
        
        let downloadGroup = DispatchGroup()
        
        //To prevent Sign-In pop up to apear at app start, i make sure that there is an actual receipt to refresh beacuse for every 'verifyRecipt' call, i first make a refresh receipt call to have the latest available receipt
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {

                downloadGroup.enter()
                AppStoreReceiptManager.shared.verifyReceipt {
                    if !UserDataManager.shared.purchasedProVersion() {
                        UserDataManager.shared.deleteAllExceeingNotificationsAndPortfolios()
                    }
                    else { // User is pro so remove the ads
                        NotificationCenter.default.post(name: Notification.Name.removeAds, object: nil)
                    }
                    downloadGroup.leave()
                }
        }
        else {
            print("No Apple Receipt Yet")
        }

        // Hot fix no server
//            downloadGroup.enter()
//            NetworkManager.shared.getSettings { [weak self] (error) in
//                if error != nil {
//                    //self?.platformDataError = error // Hot fix no server
//                }
//                downloadGroup.leave()
//            }
//
            downloadGroup.enter()
            NetworkManager.shared.getSupportedExchanges { [weak self] (error) in
                if error != nil {
                   // self?.platformDataError = error
                }
                downloadGroup.leave()
            }
            
            downloadGroup.enter()
            NetworkManager.shared.builedImagesDictFromCryptoCompere { [weak self] (error,dict) in
                if error != nil {
                    self?.platformDataError = error
                }
                else if let dict = dict {
                    UserDataManager.shared.imagesDict = dict
                }
                
                downloadGroup.leave()
            }
            
            downloadGroup.enter()
            NetworkManager.shared.getAllCoins(onCompletion: { (allCoins) in
                chaceManager.shared.allCoins = allCoins
                downloadGroup.leave()
            }) { [weak self] (error) in
                    self?.platformDataError = error
                    downloadGroup.leave()
            }
            
            downloadGroup.notify(queue: DispatchQueue.main) { [weak self] in
                if let platformDataError = self?.platformDataError {
                   
                        self?.hideLoading()
                        
                        UserDefaults.standard.set(false, forKey: Consts.platformDataFetched)
                        
                        Analytics.logEvent("platform_Data_Error", parameters: ["Message": platformDataError.localizedDescription])
                        
                        self?.platformDataError = nil
                        
                        let alertController = UIAlertController(title: "Temporary Error Fetching App Settings", message: "Try again in a few moments", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Retry", style: .default, handler: { [weak self] (action) in
                            DispatchQueue.main.async {
                                self?.getAllPlatformData()
                            }
                        }))
                        self?.present(alertController, animated: true, completion: nil)
                }
                else {
                        UserDefaults.standard.set(true, forKey: Consts.platformDataFetched)
                        self?.getFirstPageOfCoinsAndStartTimer()
                }
            }
    }
    
    func getFirstPageOfCoinsAndStartTimer() {
          lastRefreshDate = Date()
          getCoinsDataAndReloadTable(page: 0)
    }
    
    func startTick() {
           timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.tik), userInfo: nil, repeats: true)
        print("startTick")
    }
    
    func stopTick() {
        timer?.invalidate()
        print("stopTick")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadComplete), name: Notification.Name.loadComplete, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(priceUpdate(notification:)), name: Notification.Name.priceMessage, object: nil)

        
        subscribeToOnScreenCoins()
        
    
    }
    
    
    
     override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
         
         showOnboardingIfNeeded()

     }
    
    func showOnboardingIfNeeded() {
        
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            if settings.authorizationStatus == .notDetermined {
                //SHOW OnBoarding
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    let nav_onboarding = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "nav_onboarding")
                    nav_onboarding.modalPresentationStyle = .fullScreen
                    self.present(nav_onboarding, animated: true, completion: nil)
                }
            }else if #available(iOS 14, *) {
                ATTrackingManager.requestTrackingAuthorization { settings in
                }
            }
                
            
        }

    }
    

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name.loadComplete, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.priceMessage, object: nil)

       // WSManager.shared.unSubscribe(subs: ccSubscriptions) Avi zaza, not using ws at this point
    }
    
    
    // MARK: SELECTORS
    @objc func loadComplete() {
        subscriptionsLoadComplete = true
    }
    
    @objc func priceUpdate(notification: NSNotification) {
        
        guard let message = notification.userInfo?["message"] as? CCPriceMessage else {
           return
        }

        if subscriptionsLoadComplete {
            for (index,coin) in dataSource.enumerated() {
                if (coin.nameID == message.symbol) {

                    if !tableView.isDecelerating && !tableView.isDragging {
                        self.dataSource[index].updatePrice(price: message.price.significantDigitFormatWithCurrencySign())
                        tableView.reloadData()
                        
                    }


                }
            }
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification){
        if notification.name.rawValue == "applicationDidBecomeActive" {
            print("$$$$$$ applicationDidBecomeActive ")
        }
        else if notification.name.rawValue == "applicationDidEnterBackground" {
            print("$$$$$$ applicationDidEnterBackground")
        }
    }
    
    func getCoinsDataAndReloadTable(page : Int) {
        
        NetworkManager.shared.getTopList(page : page) { [weak self] (error,arr) in
                guard let self = self else {return}
                
                self.hideLoading()

                if let error = error {
                    let alertController = UIAlertController(title: "Error", message: "Failed getting top list\n\n\(error.localizedDescription)", preferredStyle: .alert)

                    alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    alertController.addAction(UIAlertAction(title: "Retry", style: .default, handler: { (action) in
                        self.showLoading()
                        self.getFirstPageOfCoinsAndStartTimer()
                    }))
                    self.present(alertController, animated: true, completion: nil)
                    
                    // Just a temporary event log to find out why somtimes this request returns an error
                    Analytics.logEvent("getTopList", parameters: ["Message":error.localizedDescription])
                    return
                }

            
                guard let arr = arr else {
                    let alertController = UIAlertController(title: "Error", message: "Array of topList objects wasn't passed", preferredStyle: .alert)
                    self.present(alertController, animated: true, completion: nil)
                    
                    return
                }
            
                if page == 0 {
                    self.dataSource =  arr
                }
                else {
                    let newArray = self.dataSource + arr
                    self.dataSource =  newArray
                }
                
                    
                chaceManager.shared.allCoinArray = self.dataSource // i will use this in other controllers
                
                let currency = UserDataManager.shared.userCurrency.value
                let currencySign = UserDataManager.shared.currencySigns[currency]!
            
                let marketCap = Int(UserDataManager.shared.totalMarketCap)
                let vol = Int(UserDataManager.shared.totalVol)
                self.totalMarketCapLabel.text = "\(currencySign)\(marketCap.withCommas())"
                self.totalVolLabel.text = "\(currencySign)\(vol.withCommas())"
                
                self.tableHeaderView.isHidden = false
                
                //reload tableView only when search not active
                self.tableView.reloadData()
                self.isLoadingPage = false
                self.subscribeToOnScreenCoins()
                
                
            
        }
    }
    
    @objc func tik() {
        print("rates VC tik")
        if(!tableView.isDecelerating) { // Makes sure that the tableView is not in moving state
            updatePricesForVisibleCells()
        }
    }
    
    func updatePricesForVisibleCells() {

        let fsymbols = fromSymbolsAsCommsSeperatedString()
        
        if fsymbols.count > 0 {
            let tsym = UserDataManager.shared.userCurrency.value
            NetworkManager.shared.getPricesFor(fsyms: fsymbols, tsym: tsym) { [weak self] (prices, error) in
                if error == nil {
                    guard let dataSource = self?.dataSource else { return }
                    var indexPathsToUpdate: [IndexPath] = []
                    
                    for  (index, coinRate) in dataSource.enumerated() { // Looping trough all the dataSource to find the specifice objects that needs to be updated with the new price
                        
                        if let newPrice = prices[coinRate.nameID] {
//                            let newPriceString = String(newPrice)
                            let newPriceString = newPrice.significantDigitFormatWithCurrencySign()

                            if (coinRate.price != newPriceString) {
                                self?.dataSource[index].updatePrice(price: newPriceString)
                                indexPathsToUpdate.append(IndexPath(row: index, section: 0))
                            }
                            
                        }
                    }
                    
                    self?.tableView.reloadRows(at: indexPathsToUpdate, with: .fade)
                }
                
            }
        }

        
    }
    
    func fromSymbolsAsCommsSeperatedString() -> String {
        
        var fsymbols = ""
        let visibleCells = tableView.visibleCells

        for (index,cell) in visibleCells.enumerated() {
            if let indexPath = tableView.indexPath(for: cell) {
                
                let coinRate = dataSource[indexPath.row]
                if index == 0 { // First object
                    fsymbols.append(contentsOf: coinRate.nameID)
                }
                else {
                    fsymbols.append(contentsOf: ",\(coinRate.nameID)")
                }
            }
        }
        
        return fsymbols
    }
    
    //MARK: ************************ UITableView *************************************

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //let green : UIColor = UIColor(red:0/255.0, green:225/255.0, blue:0/255.0, alpha: 1.0)
       // let red : UIColor = UIColor(red:255/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0)
                
        let cell = tableView.dequeueReusableCell(withIdentifier: "OneCoinRateCell") as! OneCoinRateCell
        let oneCoinRate = self.dataSource[indexPath.row]
        cell.configureCell(coinRate: oneCoinRate,rank: indexPath.row + 1)
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "OneCoinRateCell") as! OneCoinRateCell
//        cell.iconImage.kf.cancelDownloadTask()
//
//    }
    
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        selectedCoin = dataSource[indexPath.row]
        self.performSegue(withIdentifier: "CoinVCSegue", sender: self)
    }
    
    //MARK:Edit
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        let cell = tableView.cellForRow(at: editActionsForRowAt) as! OneCoinRateCell
        let green : UIColor = UIColor(red:0/255.0, green:225/255.0, blue:0/255.0, alpha: 1.0)
        let red : UIColor = UIColor(red:255/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0)
        var oneCoin = self.dataSource[editActionsForRowAt.row]
        
        
        var bgImage = UIImage()
        let random =  Int(arc4random_uniform(5)) //values from 0..4
        
        switch random {
        case 0:
            bgImage = #imageLiteral(resourceName: "shareBg")
        case 1:
            bgImage = #imageLiteral(resourceName: "ShareBG3-1")
        case 2:
            bgImage = #imageLiteral(resourceName: "ShareBG2")
        case 3:
            bgImage = #imageLiteral(resourceName: "ShareBg3")
        case 4:
            bgImage = #imageLiteral(resourceName: "ShareBG7")
        default:
            bgImage = #imageLiteral(resourceName: "shareBg")
        }
        
        
        let share = UITableViewRowAction(style: .normal, title: "Share") { [weak self]action, index in
            print("share button tapped")
            
            let w = 600
            let h = 400
            let v = ShareItem(frame :CGRect(x: 0, y: 0, width: w, height: h))
            
            v.bgImage.image = bgImage
            
            v.nameLabel.text = oneCoin.name
            v.CoinImageView.image = cell.iconImage.image
            v.priceLabel.text = "\(oneCoin.price.priceFormat)"
            v.priceView.backgroundColor = red
            v.priceView.layer.cornerRadius = 6
            //v.priceView.layer.borderWidth = 4
            
            let currency = UserDataManager.shared.userCurrency.value
            let currencuSign = UserDataManager.shared.currencySigns[currency]!
            let marketCapFloat = Float(oneCoin.marketCap)
            if let marketCapFloat = marketCapFloat {
                let marketCap = Int(marketCapFloat)
                
                v.capLabel.text = "\(currencuSign)\(marketCap.withCommas())"
            }
            
            let volFloat = Float(oneCoin.volume24Hour)
            if let volFloat = volFloat {
                let vol = Int(volFloat)
                v.volLabel.text = "\(currencuSign)\(vol.withCommas())"
            }
            
            v.rowView.layer.cornerRadius = 10
            v.rowView.layer.borderWidth = 10
            v.presentLabel.text = "\(oneCoin.percent_change_24h)%"
            
            if let presnt = Float(oneCoin.percent_change_24h) {
                
                if presnt >= 0.0 {
                    //green
                    v.priceView.backgroundColor = green
                    v.presentLabel.textColor = green
                    v.priceLabel.textColor = .black
                    //v.priceLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
                    v.arrowImageView.image = #imageLiteral(resourceName: "arrow_up")
                    v.rowView.layer.borderColor = green.cgColor
                }
                else {
                    //red
                    v.priceView.backgroundColor = red
                    v.presentLabel.textColor = red
                    v.priceLabel.textColor = .white
                    //cell.priceBGView.layer.borderColor = red.cgColor
                    v.arrowImageView.image = #imageLiteral(resourceName: "arrow_down")
                    v.rowView.layer.borderColor = red.cgColor
                }
            }
            
            v.layoutSubviews()
            //create an image from the big view
            let shareImage = UIImage(view: v)
            // let shareText = "#CoinData"
            
            
            //Share
            let activity = UIActivityViewController(activityItems: [MyStringItemSource("my custom text"),shareImage], applicationActivities: nil)
            activity.excludedActivityTypes = [
                UIActivityType.assignToContact,
                UIActivityType.print,
                UIActivityType.addToReadingList,
                UIActivityType.saveToCameraRoll,
                UIActivityType.openInIBooks,
                UIActivityType.mail,
                UIActivityType.copyToPasteboard
            ]
            
            activity.completionWithItemsHandler = { activity, success, items, error in
                print("activity: \(activity), success: \(success), items: \(items), error: \(error)")
                
                if (success) {
                    AppStoreReviewManager.requestReviewIfAppropriate()
                }
            }
            
            self?.present(activity, animated: true, completion: nil)
        }
        share.backgroundColor = .blue
        

        
        return [share]
    }
    
    
    var isLoadingPage = false
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //paging
        let lastElement = dataSource.count - 1
        if !isLoadingPage && indexPath.row == lastElement {
            // indicator.startAnimating()
            isLoadingPage = true
            
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
           // spinner.tintColor = .white
            spinner.color = .white
            spinner.startAnimating()
            spinner.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 44)
            
            self.tableView.tableFooterView = spinner
            self.tableView.tableFooterView?.isHidden = false
            
            pageIndex += 1
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                self.getCoinsDataAndReloadTable(page: self.pageIndex)
            }
        }
    }
    
    // MARK: - ScrollView
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("scrollViewDidEnd Dragging")
        if !decelerate {
            subscribeToOnScreenCoins()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEnd Decelerating")
        subscribeToOnScreenCoins()
    }

    var lastRefreshDate : Date?
    @objc private func refreshControlValueChanged(_ sender: Any) {
        
        if let date = lastRefreshDate {
            let date1 = date.addingTimeInterval(15)
            let date2 = Date()

            if date1 < date2 {
                getCoinsDataAndReloadTable(page:0)
                lastRefreshDate = Date()
            }
        }
        
           DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            self.refreshControl.endRefreshing()
            //self?.activityIndicatorView.stopAnimating()
        }
    }
    
 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "CoinVCSegue" {
            let vc = segue.destination as! CoinVC
            if let coin = selectedCoin {
                vc.coin = coin
            }
            else {
                //error
            }
        }
       
    }
    

    //MARK: SearchBar
 
    
    func setNavToNonSearchMode() {
        
        self.navigationItem.titleView = nil
        self.navigationItem.title = "Market"
        let image = UIImage(named: "magnifyingglass")
        
        let userCurrency = UserDataManager.shared.userCurrency.value

        let searchItem = UIBarButtonItem(image: image, style:.plain, target: self, action:  #selector(showChooseCoinVC))
        currencySelectionItem = UIBarButtonItem(title: userCurrency, style: .plain, target: self, action: #selector(selectCurrency))
        
        navigationItem.leftBarButtonItem = searchItem
        navigationItem.rightBarButtonItem = currencySelectionItem
    }
    
    @objc func showChooseCoinVC() {
        
        let chooseCoinNavController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseCoinNavController") as! UINavigationController
        
        let chooseCoinTVC = chooseCoinNavController.viewControllers.first as! ChooceCoinTVC
        chooseCoinTVC.delegate = self
        
        self.present(chooseCoinNavController, animated: true, completion: nil)
    }
    
    func didChooseCoin(_ coin : ZZCoin) {
        print("did choose coin: \(coin.symbol)")
        
        NetworkManager.shared.getCoinsByNames(fsyms: coin.symbol) { (arr) in
            
            DispatchQueue.main.async {
                
                
                if let coinRate = arr.first {
                    
                    self.dismiss(animated: false, completion: nil)

                    
                    let coinVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CoinVC") as! CoinVC
                    coinVC.coin = coinRate
                    self.navigationController?.pushViewController(coinVC, animated: true)
                }
                else {
                    //show error alert
                    self.dismiss(animated: false, completion: nil)
                    self.showNoDataError()
                }
            }
            
        }
    }

    func showNoDataError() {
        let alert = UIAlertController(title: "Error", message: "No Data", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
           let delegate = UIApplication.shared.delegate as! AppDelegate
             delegate.window?.rootViewController?.present(alert, animated: true)
    }

    
    // MARK: Logic
    func subscribeToOnScreenCoins() {
        // 1. If there are any old subscription, unsubscribe to them and remove them from the 'ccSubscriptions' array
        // 2. Iterate trough the visible cells to create new subscription strings and add them to ccSuibscription array
        // 3. subscribe to these new subscriptions
        
        subscriptionsLoadComplete = false
        // 1.
        if ccSubscriptions.count != 0 {
            //WSManager.shared.unSubscribe(subs: ccSubscriptions) Avi zaza, not using ws at this point
            ccSubscriptions.removeAll()
        }
        
        // 2.
        if tableView.visibleCells.count > 0 {
            tableView.visibleCells.forEach { (cell) in
                if let indexPath = tableView.indexPath(for: cell) {
                    let coin = dataSource[indexPath.row]
                    ccSubscriptions.append(coin.ccSubscriptionString)
                }
            }
            
            // 3.
            // WSManager.shared.subscribe(subs: ccSubscriptions) Avi zaza, not using ws at this point
        }
        
    }

}
extension UIViewController {
    
    func showLoading() {
  
        //remove old loading if there one
        hideLoading()
        
        //add new one
        let loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        loadingIndicator.center = self.view.center
        loadingIndicator.hidesWhenStopped = true
        //loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        loadingIndicator.tag = 6666
        self.view.addSubview(loadingIndicator)
    }
    
    func hideLoading() {
        let loadingIndicator = self.view.viewWithTag(6666)
        if let loadingIndicator = loadingIndicator as? UIActivityIndicatorView {
            loadingIndicator.stopAnimating()
            loadingIndicator.removeFromSuperview()
        }
    }
}

extension String {
    var priceFormat: String {
        let floatVal =  (self as NSString).floatValue
        if floatVal > 10 {
            let arr = self.split(separator: ".")
            guard arr.count == 2 else {return self}
            let firstVal :String  = "\(arr.first ?? "0" )"
            let lastVal  :String  = String("\(arr.last ?? "0" )".prefix(2))
            
            return firstVal + "." + lastVal
        }
        
        return  self
    }
}

extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
}

extension RatesVC : UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

extension UIViewController {
  func alert(message: String, title: String = "") {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(OKAction)
    self.present(alertController, animated: true, completion: nil)
  }
}

//extension String {
//    func numberWithCommas() -> String {
//        let numberFormatter = NumberFormatter()
//        numberFormatter.numberStyle = NumberFormatter.Style.decimal
//        return numberFormatter.string(from: NSNumber(value:self))!
//    }
//}




