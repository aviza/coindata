//
//  UIViewController.swift
//  ZZBitRate
//
//  Created by Oren on 19/03/2021.
//  Copyright © 2021 aviza. All rights reserved.
//

import Foundation
import UIKit

@nonobjc extension UIViewController {
    func add(_ child: UIViewController, frame: CGRect? = nil) {
        addChildViewController(child)

        if let frame = frame {
            child.view.frame = frame
        }

        view.addSubview(child.view)
        child.didMove(toParentViewController: self)
    }

    func remove() {
        willMove(toParentViewController: nil)
        view.removeFromSuperview()
        removeFromParentViewController()
    }
}
