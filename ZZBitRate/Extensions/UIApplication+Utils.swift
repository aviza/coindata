//
//  UIApplication+Utils.swift
//  ZZBitRate
//
//  Created by oren shalev on 05/01/2021.
//  Copyright © 2021 aviza. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    static var appVersion: String? {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }
}
