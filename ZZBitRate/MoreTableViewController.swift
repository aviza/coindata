//
//  MoreTableViewController.swift
//  ZZBitRate
//
//  Created by Oren on 04/04/2021.
//  Copyright © 2021 aviza. All rights reserved.
//

import UIKit
import MessageUI

class MoreTableViewController: UITableViewController, UIActivityItemSource, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var purchaseButton: UIButton!
    @IBOutlet weak var shareTVCell: UITableViewCell!
    @IBOutlet weak var rateUsTVCell: UITableViewCell!
    @IBOutlet weak var contactUsTVCell: UITableViewCell!
    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        purchaseButton.layer.cornerRadius = purchaseButton.frame.size.height / 2
        versionLabel.text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    }

    // MARK: IBACtion
    @IBAction func purchaseButtonTapped(_ sender: Any) {
        if let purchaseTVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PurchaseTVC") as? PurchaseTVC {
            navigationController?.pushViewController(purchaseTVC, animated: true)
        }
    }
    
    
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cell = tableView.cellForRow(at: indexPath)
        if cell == shareTVCell {
            shareApp()
        }
        else if cell == rateUsTVCell {
            rateUs()
        }
        else if cell == contactUsTVCell {
            openEmail()
        }
    }
    
    
    
    // MARK: UIActivityItemSource
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return URL(string: "https://apps.apple.com/app/crypto-alert-by-coindata/id1329509630")!
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType?) -> Any? {
        return URL(string: "https://apps.apple.com/app/crypto-alert-by-coindata/id1329509630")!
    }
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return "Coin Data App Link"
    }
    func activityViewController(_ activityViewController: UIActivityViewController, thumbnailImageForActivityType activityType: UIActivityType?, suggestedSize size: CGSize) -> UIImage? {
        return UIImage(named: "AppLogo")
    }
    
    
    // MARK: Custom Logic
    func shareApp() {
        let items = [self,UIImage(named: "AppLogo")]
        let ac = UIActivityViewController(activityItems: items as [Any], applicationActivities: nil)
        present(ac, animated: true)
    }
    
    func rateUs() {
        var components = URLComponents(url: URL(string: "https://apps.apple.com/us/app/crypto-alert-by-coindata/id1329509630")! , resolvingAgainstBaseURL: false)

        components?.queryItems = [
          URLQueryItem(name: "action", value: "write-review")
        ]

        guard let writeReviewURL = components?.url else {
          return
        }

        UIApplication.shared.open(writeReviewURL)
    }

    func openEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["coin.data.app@gmail.com"])

            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    
    // MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        dismiss(animated: true) {}
        
        if result == .sent {
            let alert = UIAlertController(title: "E-Mail Sent Successfuly", message: nil, preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true)
        }
    }
}
