//
//  SelectCurrencyVC.swift
//  ZZBitRate
//
//  Created by Oren on 19/03/2021.
//  Copyright © 2021 aviza. All rights reserved.
//

import UIKit
import RxRelay
import RxSwift

protocol SelectCurrencyVCDelegate: NSObject {
    func didSelectCurrency()
}

class SelectCurrencyVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    weak var delegate: SelectCurrencyVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self

//        tableView.layer.cornerRadius = 16
//        tableView.layer.borderWidth = 0.5
//        tableView.layer.borderColor = UIColor.white.cgColor
        tableView.backgroundColor = .clear
        self.view.backgroundColor = .clear
        
//
        
        tableView.separatorColor = UIColor.black

        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.bringSubview(toFront: tableView)
      
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return UserDataManager.shared.currencyOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        let currency = UserDataManager.shared.currencyOptions[indexPath.row]
        cell?.textLabel?.textColor = UIColor.systemOrange
        cell!.textLabel?.text = currency
        cell?.backgroundColor = UIColor.clear
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newCurrency = UserDataManager.shared.currencyOptions[indexPath.row]
        if newCurrency != UserDataManager.shared.userCurrency.value {
            UserDefaults.standard.setValue(newCurrency, forKey: Consts.userCurrency)
            UserDataManager.shared.userCurrency.accept(newCurrency)
//            NotificationCenter.default.post(name: Notification.Name.userCurrencyChanged, object: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
