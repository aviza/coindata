//
//  OnBoardingNotifications.swift
//  ZZBitRate
//
//  Created by Avi Sapir on 17/03/2022.
//  Copyright © 2022 aviza. All rights reserved.
//

import UIKit

class OnBoardingNotificationsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func skipButtonTapped(_ sender: Any) {
        self.moveToNextScreen()
        UNUserNotificationCenter.current()
          .requestAuthorization(options: [.alert, .sound, .badge]) {
            [weak self] granted, error in
        }

    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        
        UNUserNotificationCenter.current()
          .requestAuthorization(options: [.alert, .sound, .badge]) {
            [weak self] granted, error in
              self?.moveToNextScreen()
        }
    }
    
    
    func moveToNextScreen() {
        
        DispatchQueue.main.async {
            
            if #available(iOS 14, *) {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingATT_VC")
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                //WE DONT HAVE ATT below iOS 14
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingAllDoneVC")
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
        
       
    

}
