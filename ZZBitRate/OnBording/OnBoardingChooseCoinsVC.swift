//
//  OnBoardingChooseCoinsVC.swift
//  ZZBitRate
//
//  Created by Avi Sapir on 17/03/2022.
//  Copyright © 2022 aviza. All rights reserved.
//

import UIKit
import Kingfisher


class OnBoardingChooseCoinsTableViewCell:UITableViewCell {
    
    @IBOutlet weak var coinImage: UIImageView!
    @IBOutlet weak var circleImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var symbolLabel: UILabel!
}

struct onBoardingCoin {
    let symbol : String
    let name   : String
}

class OnBoardingChooseCoinsVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
 
    let dataSource: Array<onBoardingCoin> = [
                      onBoardingCoin(symbol: "BTC", name: "Bitcoin"),
                      onBoardingCoin(symbol: "ETH", name: "Ethereum"),
                      onBoardingCoin(symbol: "ADA", name: "Cardano"),
                      onBoardingCoin(symbol: "SOL", name: "Solana"),
                      onBoardingCoin(symbol: "AVAX", name: "Avalanche"),
                      onBoardingCoin(symbol: "DOT", name: "Polkadot"),
                      onBoardingCoin(symbol: "DOGE", name: "Dogecoin"),
                      onBoardingCoin(symbol: "SHIB", name: "Shiba Inu"),
                      onBoardingCoin(symbol: "LTC", name: "Litecoin"),
                      onBoardingCoin(symbol: "TRX", name: "TRON"),
                      onBoardingCoin(symbol: "XMR", name: "Monero"),
                      onBoardingCoin(symbol: "WAVES", name: "Waves"),
                      onBoardingCoin(symbol: "AAVE", name: "Aave")]
    
    var selectedIndexes:Array<Int> = []
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func skipButtonTapped(_ sender: Any) {
        moveToNextScreen()
    }
    
    @IBAction func nextBottonTapped(_ sender: Any) {
        moveToNextScreen()
    }
    
    func moveToNextScreen() {
        addSelected()
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingNotificationsVC")

        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func addSelected() {
        
        let defaults = UserDefaults.standard
        var favArray = defaults.object(forKey: "fav_array") as? [String] ?? [String]()
        
        for num in selectedIndexes {
            let symbol = dataSource[num].symbol
            if favArray.contains(symbol) {
            //do nothing
            }
            else {
                //Add new
                favArray.append(symbol)
            }
        }
        
        //save
        defaults.set(favArray, forKey: "fav_array")
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndexes.contains(indexPath.row) {
            //REMOVE
            if let index = selectedIndexes.firstIndex(of: indexPath.row) {
                selectedIndexes.remove(at: index)
            }
            
        }else {
            //ADD
            
            selectedIndexes.append(indexPath.row)
            if selectedIndexes.count == 3 {
                moveToNextScreen()
            }
        }
        
        self.tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "OnBoardingChooseCoinsTableViewCell") as! OnBoardingChooseCoinsTableViewCell
        //cell.coinImage.image = UIImage(named: "BitcoinLogo")
        cell.nameLabel.text = dataSource[indexPath.row].name
        cell.symbolLabel.text = dataSource[indexPath.row].symbol
        if selectedIndexes.contains(indexPath.row) {
            cell.circleImage.image = UIImage(named: "circle_fill")
        }else {
            cell.circleImage.image = UIImage(named: "circle")
        }
        
        let imageUrl = UserDataManager.shared.imagesDict[dataSource[indexPath.row].symbol]
        if let imageUrl = imageUrl {
            
            let url = URL(string: imageUrl)
            cell.coinImage.kf.setImage(with: url)
            // Make Image Corners Rounded
            cell.coinImage.makeRoundCorners()

        }
        
        return cell
    }
    

   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
}
