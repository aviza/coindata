//
//  OnBoardingWelcomVC.swift
//  ZZBitRate
//
//  Created by Avi Sapir on 17/03/2022.
//  Copyright © 2022 aviza. All rights reserved.
//

import UIKit

class OnBoardingWelcomVC: UIViewController {

    @IBOutlet weak var titleTopConstarint: NSLayoutConstraint!
    @IBOutlet weak var subTitleTopConstarint: NSLayoutConstraint!
    @IBOutlet weak var letsStartButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        letsStartButton.layer.cornerRadius = 10
        
    }
    
    @IBAction func letsStartButtonTapped(_ sender: Any) {
        titleTopConstarint.constant = -200
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn) {
            self.view.layoutIfNeeded()
        } completion: { finish in
            self.subTitleTopConstarint.constant = -200
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn) {
                self.view.layoutIfNeeded()
            } completion: { finish in
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingChooseCoinsVC")
    
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }

        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
