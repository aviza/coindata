//
//  OnBoardingAllDoneVC.swift
//  ZZBitRate
//
//  Created by Avi Sapir on 17/03/2022.
//  Copyright © 2022 aviza. All rights reserved.
//

import UIKit

class OnBoardingAllDoneVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.moveTonextScreen()
        }
    }
    
    func moveTonextScreen() {
        DispatchQueue.main.async {
        self.dismiss(animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
