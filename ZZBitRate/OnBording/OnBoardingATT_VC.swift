//
//  OnBoardingATT_VC.swift
//  ZZBitRate
//
//  Created by Avi Sapir on 17/03/2022.
//  Copyright © 2022 aviza. All rights reserved.
//

import UIKit
import AppTrackingTransparency


class OnBoardingATT_VC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        ask()
    }
    
    @IBAction func skipButtonTapped(_ sender: Any) {
        ask()
    }
    
    func ask() {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                switch status {
                case .notDetermined:
                    print("notDetermined")
                    break
                case .restricted:
                    print("restricted")
                    break
                case .denied:
                    print("denied")
                    break
                case .authorized:
                    print("authorized")
                    break
                }
                
                self.moveTonextScreen()
                
            }
        } else {
            // Fallback on earlier versions
            self.moveTonextScreen()

        }
    }
    
  
    
    
    func moveTonextScreen() {
        DispatchQueue.main.async {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OnBoardingAllDoneVC")
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
