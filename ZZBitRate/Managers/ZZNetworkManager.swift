//
//  ZZNetworkManager.swift
//  ZZBitRate
//
//  Created by Avi Sapir on 29/11/2021.
//  Copyright © 2021 aviza. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class ZZNetworkManager {
    static let shared = ZZNetworkManager()
    
   // static let zazaServerBaseUrl = "http://localhost:8081"
    
    //static let zazaServerBaseUrl = "http://192.168.1.100:8081"
  static let zazaServerBaseUrl = "https://coin-data-heroku.herokuapp.com"


    func ping() {
        let url = "\(ZZNetworkManager.zazaServerBaseUrl)/ping"
        AF.request(url).response { response in
            print("ping : \(response)")
        }
    }
    
    func createUser() {
        print("createUser ...  ")

        let url = "\(ZZNetworkManager.zazaServerBaseUrl)/user/create"
        AF.request(url).responseDecodable(of: UserRespaonseModel.self) { response in
            print("createUser  end ... \(response) ")
            guard let userRespaonseModel = response.value else { return }
            let user_id = userRespaonseModel.user_id
            
            //SAVE USER
            print("createUser New user created with user_id \(user_id)")
            UserDefaults.standard.setValue(user_id, forKey: "user_id")
            
            let user = User()
            let realm = try! Realm()
            try! realm.write() {
                
                user.userId = "\(user_id)"
                realm.add(user)
                print("user save to realm")
            }
            
        }
    }
    
    struct TokenParameters: Encodable {
        let user_id: Int32
        let push_token: String
    }
    
    
    
    func updateUserPushToken(token: String) {
        guard token.count > 4 else {return}
        
        
        let realm = try! Realm()
        try! realm.write({
            User.user()?.token = token
        })
        
        
        guard let userId = User.user()?.userId  else {return}
        //guard let user_id = UserDefaults.standard.value(forKey: "user_id") else {return}
        
        print("updateUserPushToken user_id :  \(userId) ... token : \(token)  ")
        let parameters = TokenParameters(user_id: Int32(userId)!, push_token: token)
        

        let url = "\(ZZNetworkManager.zazaServerBaseUrl)/user/updateToken"
        //let headers: HTTPHeaders =  ["Content-Type":"application/json"]
        
        
        AF.request(url,
                   method: .post,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default).response { response in
            
                let status = response.response?.statusCode
                if status == 200 {
                    UserDefaults.standard.setValue(token, forKey: "myFCMToken")
                    print("onSuccess")
                }else {
                    print("onFailure")
                }
        }
//        let val = AF.request(url, method: .post, parameters: parameters,headers: headers).responseDecodable(of: UserRespaonseModel.self) { response in
//            UserDefaults.standard.setValue(token, forKey: "push_token")
//            print("updateUserPushToken  end ... \(response) ")
//            guard let userRespaonseModel = response.value else { return }
//            let user_id = userRespaonseModel.user_id
//            print("updateUserPushToken user_id \(user_id)")
//        }

    }
    
    struct LimitNotificationParameters: Encodable {
   
        let user_id    :Int32
        let direction  :String
        let tsym       :String
        let fsym       :String
        let limit      :Double
        let date       :String
        let isPro      :Bool
        let exchange   :String
    }
    
    struct LimitNotificationUpdateParameters: Encodable {
   
        let _id    :String
        let user_id    :Int32
        let direction  :String
        let tsym       :String
        let fsym       :String
        let limit      :Double
        let date       :String
        let isPro      :Bool
        let exchange   :String
    }
    
    func createLimitNotification(limitNotification: LimitNotification? = nil,
                            onSuccess: @escaping StringResponse, onFailure: @escaping ErrorResponse ) {
       
        // Can't execute the request if a user doesn't exists

        let isPro = false// TODO: ?????
        guard let userId = User.user()?.userId  else {
            let error = NSError(domain: "createNotification", code: 1, userInfo: [NSLocalizedDescriptionKey:"User not exists"])
            onFailure(error)
            return
        }

        
        let parameters = LimitNotificationParameters(user_id: Int32(userId)!,
                                                       direction: limitNotification!.direction ,
                                                       tsym: limitNotification!.tsym,
                                                       fsym: limitNotification!.fsym,
                                                       limit: limitNotification!.limit,
                                                       date: Date().myFormat,
                                                       isPro: isPro,
                                                       exchange: limitNotification!.exchange)
        
        let urlString = "\(ZZNetworkManager.zazaServerBaseUrl)/limitorderbook/insertRow"

        AF.request(urlString,
                   method: .post,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default).validate(statusCode: 200..<300)
                   .responseJSON { response in
            
                       switch response.result {
                       case .success(let value):
                           
                           if let json = value as? [String:Any] {
                           
                               
                               if let order_id = json["order_id"] {
                                   let dict = ["__t": "LimitNotification",
                                               "_id":"\(order_id)",
                                               "exchange":limitNotification!.exchange,
                                               "fsym":limitNotification!.fsym,
                                               "tsym":limitNotification!.tsym,
                                               "limit":limitNotification!.limit,
                                               "direction":limitNotification!.direction,
                                               "repeated":limitNotification!.repeated,
                                               "status":true,

                                   ] as [String : Any]
                                   
        //                           copy._id = _id
        //                           copy.exchange = exchange
        //                           copy.fsym = fsym
        //                           copy.tsym = tsym
        //                           copy.name = name
        //                           copy.limit = limit
        //                           copy.direction = direction
        //                           copy.repeated = repeated
                                   
                                   
                                   parsingManager.shared.createNotification(dict: dict)
                                   onSuccess("Notification created!")
                                   print("onSuccess")
                               }else {
                                   onFailure(NSError(domain: "createNotification", code: 1, userInfo: [NSLocalizedDescriptionKey:"no order id"]))
                               }

                        
                           }

                           
                       case .failure(let error):
                           onFailure(error)
                           print("onFailure")
                       }
                       
        }

    }
    
    func updateLimitNotification(limitNotification: LimitNotification? = nil,
                            onSuccess: @escaping StringResponse, onFailure: @escaping ErrorResponse ) {
       
        // Can't execute the request if a user doesn't exists

        let isPro = false// TODO: ?????
        guard let userId = User.user()?.userId  else {
            let error = NSError(domain: "createNotification", code: 1, userInfo: [NSLocalizedDescriptionKey:"User not exists"])
            onFailure(error)
            return
        }
        
        let parameters = LimitNotificationUpdateParameters(_id: limitNotification!._id,
                                                           user_id: Int32(userId)!,
                                                           direction: limitNotification!.direction ,
                                                           tsym: limitNotification!.tsym,
                                                           fsym: limitNotification!.fsym,
                                                           limit: limitNotification!.limit,
                                                           date: Date().myFormat,
                                                           isPro: isPro,
                                                           exchange: limitNotification!.exchange)
    
        

        
        //let urlString = "\(coinDataServiceUrl)/createNotification/\(notificationType)"
        let urlString = "\(ZZNetworkManager.zazaServerBaseUrl)/limitorderbook/updateLimitNotification"

        AF.request(urlString,
                   method: .post,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default).validate(statusCode: 200..<300)
                   .responseJSON { response in
            
                       switch response.result {
                       case .success(let value):
                           
                           if let json = value as? [String:Any] {
                           

                           let dict = ["__t": "LimitNotification",
                                       "_id":limitNotification!._id,
                                       "exchange":limitNotification!.exchange,
                                       "fsym":limitNotification!.fsym,
                                       "tsym":limitNotification!.tsym,
                                       "limit":limitNotification!.limit,
                                       "direction":limitNotification!.direction,
                                       "repeated":limitNotification!.repeated,
                                       "status":1
                           ] as [String : Any]
                           
//                           copy._id = _id
//                           copy.exchange = exchange
//                           copy.fsym = fsym
//                           copy.tsym = tsym
//                           copy.name = name
//                           copy.limit = limit
//                           copy.direction = direction
//                           copy.repeated = repeated
                           
                           
                           parsingManager.shared.createNotification(dict: dict)
                           onSuccess("Notification created!")
                           print("onSuccess")
                           }

                           
                       case .failure(let error):
                           onFailure(error)
                           print("onFailure")
                       }
                       

        }
        
        
//
//         let url = URL(string: urlString)
//         guard let requestUrl = url else { fatalError() }
//         // Prepare URL Request Object
//         var request = URLRequest(url: requestUrl)
//         request.httpMethod = "POST"
//
//
//        // Set HTTP Request Body
//         request.httpBody = postString.data(using: String.Encoding.utf8)
//        print("CreateNotification Request:\nurl: \(requestUrl)\nmethod: \(String(describing: request.httpMethod))\n postString:\(postString)")
//
//        // Perform HTTP Request
//         let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
//
//                 // Check for Error
//                 if let error = error {
//                     print("Error took place \(error)")
//                     onFailure(error)
//                     return
//                 }
//
//                 // Convert HTTP Response Data to a String
//                 if let data = data, let dataString = String(data: data, encoding: .utf8) {
//                     print("Response data string:\n \(dataString)")
             //       let success = parsingManager.shared.parseCreateNotification(data: data)
//                     if success {
//                        onSuccess("Notification created!")
//                     }
//                     else {
//                        let error = NSError(domain: "createNotification", code: 1, userInfo: [NSLocalizedDescriptionKey:"Could'nt create notification try again later"])
//                        onFailure(error)
//                     }
//
//                 }
//         }
//
//         task.resume()
    }
    
    func deleteLimitNotification(notificationId: String,
                                  onSuccess: @escaping StringResponse,
                            onFailure: @escaping ErrorResponse) {
        
        let urlString = "\(ZZNetworkManager.zazaServerBaseUrl)/limitorderbook/deleteorder/\(notificationId)"
        
        AF.request(urlString).response { response in
            onSuccess("notification deleted!")
        }

    }
    
    func deleteIntervalNotification(notificationId: String,
                                  onSuccess: @escaping StringResponse,
                            onFailure: @escaping ErrorResponse) {
        
        let urlString = "\(ZZNetworkManager.zazaServerBaseUrl)/intervalorderbook/deleteorder/\(notificationId)"
        
        AF.request(urlString).response { response in
            onSuccess("notification deleted!")
        }

    }
    
    
    struct IntervalNotificationParameters: Encodable {
   
        let user_id    :Int32
        let tsym       :String
        let fsym       :String
        let date       :String
        let isPro      :Bool
        let exchange   :String
        let interval   :Int

    }
    
    func createIntervalNotification(intervalNotification: IntervalNotification? = nil,
                            onSuccess: @escaping StringResponse, onFailure: @escaping ErrorResponse ) {
       
        // Can't execute the request if a user doesn't exists

        let isPro = false// TODO: ?????
        
        guard let userId = User.user()?.userId  else {
            let error = NSError(domain: "createNotification", code: 1, userInfo: [NSLocalizedDescriptionKey:"User not exists"])
            onFailure(error)
            return
        }

        
        let parameters = IntervalNotificationParameters(user_id: Int32(userId)!,
                                                        tsym: intervalNotification!.tsym,
                                                        fsym: intervalNotification!.fsym,
                                                        date: Date().myFormat,
                                                        isPro: isPro,
                                                        exchange: intervalNotification!.exchange,
                                                        interval: intervalNotification!.interval)
        
        let urlString = "\(ZZNetworkManager.zazaServerBaseUrl)/intervalorderbook/insertRow"

        AF.request(urlString,
                   method: .post,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default).validate(statusCode: 200..<300)
                   .responseJSON { response in
            
                       switch response.result {
                       case .success(let value):
                           
                           if let json = value as? [String:Any] {
                           
                               if let order_id = json["order_id"] {
                                   let dict = ["__t": "IntervalNotification",
                                               "_id":"\(order_id)",
                                               "exchange":intervalNotification!.exchange,
                                               "fsym":intervalNotification!.fsym,
                                               "tsym":intervalNotification!.tsym,
                                               "interval":intervalNotification!.interval,
                                               "date" :Date().myFormat,
                                               "status" :true


                                   ] as [String : Any]
                                   
                                   
                                   parsingManager.shared.createNotification(dict: dict)
                                   onSuccess("Notification created!")
                                   print("onSuccess")
                               }else {
                                   let error = NSError(domain: "createNotification", code: 1, userInfo: [NSLocalizedDescriptionKey:"No order id"])
                                   onFailure(error)
                               }

                   
                           }

                           
                       case .failure(let error):
                           onFailure(error)
                           print("onFailure")
                       }
                       
        }

    }
    
    struct IntervalNotificationUpdateParameters: Encodable {
   
        let _id    :Int
        let user_id    :Int32
        let tsym   :String
        let fsym :String
        let date       :String
        let isPro      :Bool
        let exchange   :String
        let interval : Int
    }
    
    func updateIntervalNotification(notification: IntervalNotification? = nil,
                            onSuccess: @escaping StringResponse, onFailure: @escaping ErrorResponse ) {
       
        // Can't execute the request if a user doesn't exists

        let isPro = false// TODO: ?????
        guard let userId = User.user()?.userId  else {
            let error = NSError(domain: "createNotification", code: 1, userInfo: [NSLocalizedDescriptionKey:"User not exists"])
            onFailure(error)
            return
        }
        
    
        
        
        let parameters = IntervalNotificationUpdateParameters(_id:Int(notification!._id)! ,
                                                              user_id: Int32(userId)!,
                                                              tsym: notification!.tsym,
                                                              fsym: notification!.fsym,
                                                              date: Date().myFormat,
                                                              isPro: isPro,
                                                              exchange: notification!.exchange,
                                                              interval: notification!.interval
        
//       // let parameters = LimitNotificationUpdateParameters(order_id:nofiticatin!._id,
//                                                           user_id: userId as! Int32,
//                                                       toSymbol: nofiticatin!.tsym,
//                                                       fromSymbol: nofiticatin!.fsym,
//                                                     date: Date().myFormat,
//                                                       isPro: isPro,
//                                                       exchange: nofiticatin!.exchange,
//                                                           timeInterval:nofiticatin!.interval
        )
    
        

        
        //let urlString = "\(coinDataServiceUrl)/createNotification/\(notificationType)"
        let urlString = "\(ZZNetworkManager.zazaServerBaseUrl)/intervalorderbook/updateIntervalNotification"

        AF.request(urlString,
                   method: .post,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default).validate(statusCode: 200..<300)
                   .responseJSON { response in
            
                       switch response.result {
                       case .success(let value):
                           
                           if let json = value as? [String:Any] {
                           

                           let dict = ["__t": "IntervalNotification",
                                       "_id":notification!._id,
                                       "exchange":notification!.exchange,
                                       "fsym":notification!.fsym,
                                       "tsym":notification!.tsym,
                                       "interval":notification!.interval
                           ] as [String : Any]
                           
//                           copy._id = _id
//                           copy.exchange = exchange
//                           copy.fsym = fsym
//                           copy.tsym = tsym
//                           copy.name = name
//                           copy.limit = limit
//                           copy.direction = direction
//                           copy.repeated = repeated
                           
                           
                           parsingManager.shared.createNotification(dict: dict)
                           onSuccess("Notification created!")
                           print("onSuccess")
                           }

                           
                       case .failure(let error):
                           onFailure(error)
                           print("onFailure")
                       }
                       

        }
        
        
//
//         let url = URL(string: urlString)
//         guard let requestUrl = url else { fatalError() }
//         // Prepare URL Request Object
//         var request = URLRequest(url: requestUrl)
//         request.httpMethod = "POST"
//
//
//        // Set HTTP Request Body
//         request.httpBody = postString.data(using: String.Encoding.utf8)
//        print("CreateNotification Request:\nurl: \(requestUrl)\nmethod: \(String(describing: request.httpMethod))\n postString:\(postString)")
//
//        // Perform HTTP Request
//         let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
//
//                 // Check for Error
//                 if let error = error {
//                     print("Error took place \(error)")
//                     onFailure(error)
//                     return
//                 }
//
//                 // Convert HTTP Response Data to a String
//                 if let data = data, let dataString = String(data: data, encoding: .utf8) {
//                     print("Response data string:\n \(dataString)")
             //       let success = parsingManager.shared.parseCreateNotification(data: data)
//                     if success {
//                        onSuccess("Notification created!")
//                     }
//                     else {
//                        let error = NSError(domain: "createNotification", code: 1, userInfo: [NSLocalizedDescriptionKey:"Could'nt create notification try again later"])
//                        onFailure(error)
//                     }
//
//                 }
//         }
//
//         task.resume()
    }
    
    func allNotification() {
        
       // let isPro = false// TODO: ?????
        
        guard let userId = User.user()?.userId else {
           // let error = NSError(domain: "createNotification", code: 1, userInfo: [NSLocalizedDescriptionKey:"User not exists"])
            return
        }
        
//        guard let userId = UserDefaults.standard.value(forKey: "user_id")  else {
//            let error = NSError(domain: "createNotification", code: 1, userInfo: [NSLocalizedDescriptionKey:"User not exists"])
//            return
//        }
        
        let urlString = "\(ZZNetworkManager.zazaServerBaseUrl)/user/getAllNotifications/\(userId)"
        AF.request(urlString).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let json = value as? [String:Any] {
                    parsingManager.shared.zzParseallNotifications(json: json)
                print("onSuccess")
                }
                
            case .failure(let error):
                print("onFailure \(error)")
            }
        }
        
        
        
    }
    
    struct UpdateNotificationStatusParameters: Encodable {
        let order_id: Int32
        let status: Int
    }
    
    func updateNotificationStatus(type: String, notificationId: String, status: Int,
                                  onSuccess: @escaping StringResponse,
                                  onFailure: @escaping ErrorResponse) {
        guard let order_id = Int(notificationId) else {
            let error = NSError(domain: "createNotification", code: 1, userInfo: [NSLocalizedDescriptionKey:"No order id"])
            onFailure(error)
            return
        }

        
        var urlString = ""
        if type == "LimitNotification" {
            urlString = "\(ZZNetworkManager.zazaServerBaseUrl)/limitorderbook/updateNotificationStatus"
        }
        else if type == "IntervalNotification" {
            urlString = "\(ZZNetworkManager.zazaServerBaseUrl)/intervalorderbook/updateNotificationStatus"
        }
        

        let parameters = UpdateNotificationStatusParameters(order_id: Int32(order_id),
                                                            status: status)
        
        
        
        AF.request(urlString,
                   method: .post,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default).validate(statusCode: 200..<300)
                   .responseJSON { response in
            
                       switch response.result {
                       case .success(let value):
                           if let json = value as? [String:Any] {
                               if let order_id = json["order_id"] {
                                   let dict = ["__t": type,
                                               "_id":"\(order_id)",
                                               "status" : status
                                   ] as [String : Any]
                                   
                                   parsingManager.shared.createNotification(dict: dict)
                                   onSuccess("Notification created!")
                                   print("onSuccess")
                               }else {
                                   let error = NSError(domain: "createNotification", code: 1, userInfo: [NSLocalizedDescriptionKey:"No order id"])
                                   onFailure(error)
                               }
                           }

                       case .failure(let error):
                           onFailure(error)
                           print("onFailure")
                       }
                       
        }
    
    }
    
    //MARK:Portfolio
    
    struct deletePortfolioParameters: Encodable {
        let portfolioId: String
    }
    
    func deletePortfolio(portfolioId: String,
                                  onSuccess: @escaping StringResponse,
                         onFailure: @escaping ErrorResponse) {
        
        let urlString = "\(ZZNetworkManager.zazaServerBaseUrl)/portfolio/deleteProtfolio"
        let parameters = deletePortfolioParameters(portfolioId: portfolioId)

        AF.request(urlString,
                   method: .post,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default)
            .responseJSON { response in
                
                if response.response?.statusCode == 200 {
                    let realm = try! Realm()
                    try! realm.write() {
                        
                            let portfolios = realm.objects(Portfolio.self).filter("_id = %@", portfolioId)
                            if portfolios.count > 0 {
                                realm.delete(portfolios[0])
                            }
                    }
                    
                    DispatchQueue.main.async {
                        onSuccess("portfolio deleted!")
                    }
                    
                }else {
                    
                    let error = NSError(domain: "deletePortfolio", code: 1, userInfo: [NSLocalizedDescriptionKey:"Couldn't delete portfolio"])
                    DispatchQueue.main.async {
                        onFailure(error)
                    }
                    
                }
                
            }
        
        
        
    }
    
    struct getPortfolioBalanceParameters: Encodable {
        let exchangeName: String
        let token: String
    }
    
    func getPortfolioBalance(exchangeName: String, token: String,
                            completion: @escaping ServiceResponseBalance) {
        
        let urlString = "\(ZZNetworkManager.zazaServerBaseUrl)/portfolio/getPortfolioBalance"
        let parameters = getPortfolioBalanceParameters(exchangeName: exchangeName,token:token)

        AF.request(urlString,
                   method: .post,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default)
            .responseJSON { response in
                
                switch response.result {
                case .success(let value):
                    if let json = value as? [String:Any] {
                        
                            if let balance = parsingManager.shared.parseExchangeBalance(data: json) {
                                DispatchQueue.main.async {
                                    completion(balance,nil)
                                }
                            }
                    }

                case .failure(let error):
                    let error = NSError(domain: "getBalanceForExchange", code: 1, userInfo: [NSLocalizedDescriptionKey:"failed getting balance for exchange try again later"])
                    
                    DispatchQueue.main.async {
                        completion([],error)
                    }
                    print("onFailure")
                }
               
                
            }
    }
    
    struct addCoinBalanceParameters: Encodable {
        let portfolio_id: String
        let symbol: String
        let amount: Double
    }
    
    
    func addCoinBalance(portfolioId: String, symbol: String, amount: Double,
                        completion: @escaping (_ error: Error?) -> ()) {
        
        let urlString = "\(ZZNetworkManager.zazaServerBaseUrl)/portfolio/addCoinBalance"
        let parameters = addCoinBalanceParameters(portfolio_id: portfolioId, symbol:symbol, amount:amount)

        AF.request(urlString,
                   method: .post,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default)
            .responseJSON { response in
                
                switch response.result {
                case .success(let value):
                    if let json = value as? [String:Any] {
                        
                        let coinParsed = parsingManager.shared.parseAddCoinBalance(data: json)
                        if coinParsed {
                               completion(nil)
                        }
                        else {
                           let error = NSError(domain: "addCoinBalance", code: 1, userInfo: [NSLocalizedDescriptionKey:"failed adding coin balance for portfolio try again later"])
                               completion(error)
                       }
                           
                    }

                case .failure(let error):
                    let error = NSError(domain: "getBalanceForExchange", code: 1, userInfo: [NSLocalizedDescriptionKey:"failed getting balance for exchange try again later"])
                    
                    
                    print("onFailure")
                }
               
                
            }
        
        
    }
    
    
    
}

extension Date {
    var myFormat:String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatterGet.string(from: self)
    }
}
