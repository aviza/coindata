//
//  AppStoreReceiptManager.swift
//  ZZBitRate
//
//  Created by oren shalev on 13/12/2020.
//  Copyright © 2020 aviza. All rights reserved.
//

import Foundation
import StoreKit

typealias VerifyReceiptCompletion = () -> ()

class AppStoreReceiptManager: NSObject, SKRequestDelegate {

    static let shared = AppStoreReceiptManager()
    private var request: SKReceiptRefreshRequest?
    var verifyReceiptCompletion: VerifyReceiptCompletion?
    var receipt: Receipt?
    
    func verifyReceipt(verifyReceiptCompletion: @escaping VerifyReceiptCompletion) {
            self.verifyReceiptCompletion = verifyReceiptCompletion
            refreshReceipt()
    }
    
    func restorePurchases(restoreCompletion: @escaping VerifyReceiptCompletion) {
            self.verifyReceiptCompletion = restoreCompletion
            refreshReceipt()
    }
    
   private func receiptIsInvalidOrMissing() -> Bool {
        
        guard let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) else {
                // Receipt Doesn't exist
                return true
        }
        
        do {
            let _ = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
            // Receipt exitst and  valid
            return false
        }
        catch {
            // Receipt is invalid
            return true
        }
        
    }

        
   private func refreshReceipt() {
        request?.cancel()
        request = SKReceiptRefreshRequest()
        request!.delegate = self
        request!.start()
    }
    
    func requestDidFinish(_ request: SKRequest) {
        print("App Store Receipt Updated!")
        CoinDataProducts.store.validateReceipt()
        verifyReceiptCompletion?()
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("App Store Receipt Error: \(error)")
//        CoinDataProducts.store.validateReceipt()
        verifyReceiptCompletion?()
    }
}
