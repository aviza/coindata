//
//  AnalyticsManager.swift
//  ZZBitRate
//
//  Created by oren shalev on 07/01/2021.
//  Copyright © 2021 aviza. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import FirebaseRemoteConfig

class AnalyticsManager {
    // MARK: - PurchaseTVC
    static func purchasedProduct(withIdentifier identifier: String) {
        
        var value: NSNumber = NSNumber(value: 0)
        if identifier == CoinDataProducts.everyMonthSubscription {
            if let price = RemoteConfig.remoteConfig().configValue(forKey: "monthPrice").numberValue {
               value = price
            }
        }
        else if identifier == CoinDataProducts.everyYearSubscription {
            if let price = RemoteConfig.remoteConfig().configValue(forKey: "yearPrice").numberValue {
                value = price
            }
        }
        
        UserDefaults.standard.set(true, forKey: "Marked_As_Pro")
        Analytics.setUserProperty("Pro", forName: "User_Type")
        Analytics.logEvent("Coin_Data_Purchase", parameters: [
            AnalyticsParameterItemName: identifier,
            AnalyticsParameterPrice: value,
            AnalyticsParameterCurrency: "USD",
            AnalyticsParameterValue: value
        ])
    }
    
    static func restorePurchases() {
        UserDefaults.standard.set(true, forKey: "Marked_As_Pro")
        Analytics.setUserProperty("Pro", forName: "User_Type")
        Analytics.logEvent("Coin_Data_Restore", parameters: [:])
    }
    
    static func visitedPurchaseTVC() {
        UserDefaults.standard.set(true, forKey: "Visited_Purchase_Screen")
        Analytics.setUserProperty("Visited", forName: "Visited_Purchase_Screen")
    }
    
    static func monthlySubscriptionTap() {
        Analytics.logEvent("one_month_button_tap", parameters: [:])
    }
    
    static func yearlySubscriptionTap() {
        Analytics.logEvent("one_year_button_tap", parameters: [:])
    }
    
    static func restoreTap() {
        Analytics.logEvent("restore_tap", parameters: [:])
    }
    
    // MARK: - Portfolio
    static func addNewPortfolioTap() {
        Analytics.logEvent("add_portfolio", parameters: [:])
    }
    static func selectedNewExchangePortfolio() {
        Analytics.logEvent("selected_exchange_portfolio", parameters: [:])
    }
    static func selectedNewManualPortfolio() {
        Analytics.logEvent("selected_manual_portfolio", parameters: [:])
    }
    static func addNewExchangePortfolioTap() {
        Analytics.logEvent("add_exchange_portfolio", parameters: [:])
    }
    static func addNewManualPortfolioTap() {
        Analytics.logEvent("add_manual_portfolio", parameters: [:])
    }
    static func exchangePortfolioAdded() {
        Analytics.logEvent("exchange_portfolio_added", parameters: [:])
    }
    static func manualPortfolioAdded() {
        Analytics.logEvent("manual_portfolio_added", parameters: [:])
    }
    
    // MARK - Alerts
    static func selectedIntervalAlert() {
        Analytics.logEvent("selected_interval_alert", parameters: [:])
    }
    static func selectedLimitAlert() {
        Analytics.logEvent("selected_limit_alert", parameters: [:])
    }
    
    static func addAlertTapForType(type: NotificationType) {
        switch type {
        case .IntervalNotification:
            Analytics.logEvent("add_interval_alert", parameters: [:])
        case .LimitNotification:
            Analytics.logEvent("add_limit_alert", parameters: [:])
        }
    }
    
    static func alertAddedForType(type: NotificationType) {
        switch type {
        case .IntervalNotification:
            Analytics.logEvent("interval_alert_added", parameters: [:])
        case .LimitNotification:
            Analytics.logEvent("limit_alert_added", parameters: [:])
        }
    }
    
    static func selectExchangeAndPairTap() {
        Analytics.logEvent("select_exchange_and_pair_tap", parameters: [:])
    }
    
    // MARK - Detail Screen
    static func addFavorite(coin: String) {
        Analytics.logEvent("add_favorite", parameters: ["coin": coin])
    }
    
    static func disabledNotificationOnFirstOpen() {
        Analytics.logEvent("disabled_notifications_on_start", parameters: [:])
    }
    
    
    //Avi zaza
    static func deleteUserFromLagacyVersion() {
        Analytics.logEvent("delete_user_from_lagacy_version", parameters: [:])
    }
    
}
