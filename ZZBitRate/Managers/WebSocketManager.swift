//
//  WebSocketManager.swift
//  ZZBitRate
//
//  Created by oren shalev on 03/12/2020.
//  Copyright © 2020 aviza. All rights reserved.
//

import Foundation
import Starscream

class WSManager {
    static var shared: WSManager = WSManager()
    var socket: WebSocket!
    var reconnectTimer: Timer?
    
    var tempSubs: [String] = []
    init() {
        connect() // avi zaza
    }
    
    @objc func connect()  {
        var request = URLRequest(url: URL(string: "wss://streamer.cryptocompare.com/v2?api_key=dd470f89924f82d5f63d337a001d096c550841337788ec74602293c285964060")!)
        //var request = URLRequest(url: URL(string: "wss://streamer.cryptocompare.com/v2?api_key=42fbe9bc7bab2a0623bdb38018649560e6d375a7ef9b8759ec58da0f841a33db")!)
        
                request.timeoutInterval = 5
                socket = WebSocket(request: request)
                socket.onConnect = { [unowned self] in
                    print("socket conected")
                    self.reconnectTimer?.invalidate()
                    self.subscribe(subs: self.tempSubs)
                }

                socket.onText = { text in
                    let data = text.data(using: .utf8)!
                    do {
                        if let messageDict = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String: Any]
                        {
                            if let meesageType = messageDict["TYPE"] as? String, meesageType == "3"  {
                                NotificationCenter.default.post(name: Notification.Name.loadComplete, object: nil, userInfo:nil)
                            }
                            else if let messageType = messageDict["TYPE"] as? String, messageType == "5",
                               let fromSymbol = messageDict["FROMSYMBOL"] as? String,
                               let price = messageDict["PRICE"] as? Double {
                                
                               let message = CCPriceMessage(symbol: fromSymbol, price: price)
                               NotificationCenter.default.post(name: Notification.Name.priceMessage, object: nil, userInfo: ["message": message])
                            }
                            else {
//                                print("message: \(messageDict)")
                            }
                            
                        } else {
                            print("bad json")
                        }
                    } catch let error as NSError {
                        print(error)
                    }
                }
                socket.onDisconnect = { error in
                    print("socket disconnect")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 10) { [unowned self] in
                        self.socket.connect()
                    }
                }

                socket.connect()
    }
    

    
    func subscribe(subs: [String]) {
        
        if !socket.isConnected {
           return tempSubs.append(contentsOf: subs)
        }
        
        let message: [String : Any] = [
            "action": "SubAdd",
            "subs": subs
            ]
        
        do {
            let data = try JSONSerialization.data(withJSONObject: message, options: [])
            self.socket.write(data: data)

        } catch  {
            print(error)
        }
    }
    
    
    func unSubscribe(subs: [String]) {
        if !socket.isConnected {
            for sub in subs {
                if let indexForRemove = tempSubs.index(of: sub) {
                    tempSubs.remove(at: indexForRemove)
                }
            }
        }
        
        let message: [String : Any] = [
            "action": "SubRemove",
            "subs": subs
            ]
        
        do {
            let data = try JSONSerialization.data(withJSONObject: message, options: [])
            self.socket.write(data: data)

        } catch  {
            print(error)
        }

    }
}
