//
//  ExchangesAndPairsVC.swift
//  ZZBitRate
//
//  Created by Oren on 04/03/2021.
//  Copyright © 2021 aviza. All rights reserved.
//

import UIKit

protocol ExchangesAndPairsVCDelegate: NSObject {
    func didSelectExchangeAndPair(exchange: String, fsym: String, tsym: String)
}

class ExchangesAndPairsVC: UIViewController {
    
    var symbol: String! = "BTC"
    var exchanges: [ExchangePairs] = []
    var filteredExchanges: [ExchangePairs] = []

    let searchController = UISearchController(searchResultsController: nil)
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }

    weak var delegate: ExchangesAndPairsVCDelegate?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        navigationItem.title = "Exchange - Pair"
        tableView.register(UINib(nibName: "PairCell", bundle: nil), forCellReuseIdentifier: "PairCell")
        
        setSearchController()
        
        registerToKeyBoardNotifications()
        
        if let symbol = self.symbol {
            NetworkManager.shared.getAllExchangesAndTradingPairsForSymbol(symbol: symbol) { [weak self]  (error, exchanges) in
               
                self?.activityIndicator.stopAnimating()
                
                if error != nil {
                    
                    let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                    
                }
                else {
                    print("exchanges:\(exchanges!)")

                    self?.exchanges = exchanges!
                    
                    let globalAverageItem = ExchangePairs(exchangeName: "CCCAGG", tsyms: ["USD"], isActive: true)
                    self?.exchanges.insert(globalAverageItem, at: 0)
                    self?.tableView.isHidden = false
                    self?.tableView.reloadData()
                }
    
            }
        }


    }


    
    // MARK: UISearchController
    func setSearchController() {
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        }
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Exchange"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    func filterContentForSearchText(_ searchText: String) {
      filteredExchanges = exchanges.filter { (exchange: ExchangePairs) -> Bool in
        return exchange.exchangeName.lowercased().contains(searchText.lowercased())
      }
      
      tableView.reloadData()
    }

    //MARK: KeyBoard Notifications
    func registerToKeyBoardNotifications()  {
        NotificationCenter.default.addObserver(self,
        selector: #selector(handle(keyboardShowNotification:)),
        name: NSNotification.Name.UIKeyboardWillShow,
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(handle(keyboardHideNotification:)),
        name: NSNotification.Name.UIKeyboardWillHide,
        object: nil)
    }

    @objc private func handle(keyboardShowNotification notification: Notification) {

        if let userInfo = notification.userInfo,
            let keyboardRectangle = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect {
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardRectangle.height - view.safeAreaInsets.bottom, right: 0)

        }
    }

    @objc private func handle(keyboardHideNotification notification: Notification) {

            self.tableView.contentInset = .zero
    }
}

extension ExchangesAndPairsVC: UITableViewDelegate,UITableViewDataSource {
    // TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {

        if isFiltering {
            return filteredExchanges.count
        } else {
            return exchanges.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("ExchangePairHeaderView", owner: self, options: nil)![0] as? ExchangePairHeaderView
        
        let exchanges: [ExchangePairs]
        if isFiltering {
            exchanges = filteredExchanges
        } else {
            exchanges = self.exchanges
        }
        
        let exchange = exchanges[section]
        headerView?.exchangeNameLabel.text = exchange.exchangeName == "CCCAGG" ? "Global Average" : exchange.exchangeName
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        let exchanges: [ExchangePairs]
        if isFiltering {
            exchanges = filteredExchanges
        } else {
            exchanges = self.exchanges
        }
        
        let exchange = exchanges[section]
        return exchange.tsyms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let exchanges: [ExchangePairs]
        if isFiltering {
            exchanges = filteredExchanges
        } else {
            exchanges = self.exchanges
        }
        let exchange = exchanges[indexPath.section]
        let tsym = exchange.tsyms[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PairCell", for: indexPath) as! PairCell
        cell.pairNameLabel.text = "\(symbol!) - \(tsym)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let exchanges: [ExchangePairs]
        if isFiltering {
            exchanges = filteredExchanges
        } else {
            exchanges = self.exchanges
        }
        
        let exchange = exchanges[indexPath.section]
        let tsym = exchange.tsyms[indexPath.row]
        
        self.delegate?.didSelectExchangeAndPair(exchange: exchange.exchangeName, fsym: symbol, tsym: tsym)
        navigationController?.popViewController(animated: true)
    }
}

extension ExchangesAndPairsVC: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
    filterContentForSearchText(searchBar.text!)
  }
}
