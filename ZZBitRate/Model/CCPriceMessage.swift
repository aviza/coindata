//
//  CCPriceMessage.swift
//  ZZBitRate
//
//  Created by Oren on 02/03/2021.
//  Copyright © 2021 aviza. All rights reserved.
//

import Foundation

typealias CCSubscription = String

struct CCPriceMessage {
    
    let symbol: String
    let price: Double
}
