//
//  ExchangePairs.swift
//  ZZBitRate
//
//  Created by Oren on 04/03/2021.
//  Copyright © 2021 aviza. All rights reserved.
//

import Foundation

struct ExchangePairs {

    var exchangeName: String
    var tsyms: [String] = []
    var isActive: Bool

    init(exchangeName: String, pairs: [String: Any], isActive: Bool) {
        self.exchangeName = exchangeName
        self.isActive = isActive
        
        guard let coinTSymbols = pairs.first?.value as? [String: [String: Any]]
               else {
            return
        }
        
        if let tsyms = coinTSymbols.first?.value.keys {
            
            for tsym in tsyms {
                self.tsyms.append(tsym)
            }
        }        
    }
    
    init(exchangeName: String, tsyms: [String], isActive: Bool) {
        self.exchangeName = exchangeName
        self.tsyms = tsyms
        self.isActive = isActive
    }
    
}

